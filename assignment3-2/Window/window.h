#ifndef WINDOW_H
#define WINDOW_H

#include <QWidget>
#include <QDir>
#include <QFileDialog>
#include <QDirIterator>
#include <QPluginLoader>
#include <QDebug>
#include <QThread>
#include <QDateTime>
#include <QMessageBox>

#include "../myPlugin/interface.h"


QT_BEGIN_NAMESPACE
namespace Ui { class Window; }
QT_END_NAMESPACE



class Window : public QWidget
{
    Q_OBJECT

public:
    Window(QWidget *parent = nullptr);
    ~Window();

    QStringList getPlugins(QString path);
    void testPlugins(QStringList plugins);

    Interface* plugin;

public slots:

    void on_getDir_Button_pressed();
    void on_findButton_pressed();
    void showResults();

private:

    QStringList filters;
    bool signInsert = false;
    bool sizeChecked = false;
    bool ageChecked = false;

    int size = 0;
    int age = 0;

    QFileInfoList result_gui;
    QStringList stringresult_gui;

    Ui::Window *ui;
};
#endif // WINDOW_H
