#include "window.h"
#include "ui_window.h"


Window::Window(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Window)
{
    ui->setupUi(this);

    qDebug() << "UI thread:" << QThread::currentThread();


    connect(ui->getDirButton, &QAbstractButton::clicked, this, &Window::on_getDir_Button_pressed);
    connect(ui->findButton, &QAbstractButton::clicked, this, &Window::on_findButton_pressed);

}

QStringList Window::getPlugins(QString path){
    QStringList filters;
    filters << "*.dll" << "*.so" << "*.dylib";

    QDir dir(path);
    QFileInfoList list = dir.entryInfoList(filters);

    QStringList plugins;

    foreach(QFileInfo file, list){
    if(!file.isSymLink())
     plugins.append(file.filePath());
    }

    return plugins;

}

void Window::testPlugins(QStringList plugins){
   foreach(QString file, plugins){

     qDebug() << "Loading:" << file;

     QPluginLoader loader(file);
     if(!loader.load()){
       qInfo() << "Error:" << loader.fileName() << loader.errorString();
     }

     qInfo() << "Loaded: " << loader.fileName();

     plugin = qobject_cast<Interface*>(loader.instance());

     connect(&(plugin->watcher), &QFutureWatcher<QFileInfo>::finished, this, &Window::showResults);

     connect(ui->cancelButton, &QAbstractButton::clicked, [=](){
         if (!plugin->watcher.isFinished()) {
             plugin->watcher.cancel();
             qDebug() << "work canceled:" << QThread::currentThread();
             qDebug() << plugin->watcher.isCanceled();

             QStringList currentRes;
             currentRes = plugin->getResults();
             ui->listWidget->addItem("[ Found: files - " + QString::number(currentRes.size() - plugin->countDirs) + ", dirs - " + QString::number(plugin->countDirs) + ' ]');
             ui->listWidget->addItems(currentRes);
         }
         });

     if(plugin)
        plugin->test("Connected");
     else
        qInfo() << "Plugin " << loader.fileName() << "doesn't support interface" ;

   }
}

void Window::on_getDir_Button_pressed()
{
    QString dir = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
        "/home",
        QFileDialog::ShowDirsOnly
        | QFileDialog::DontResolveSymlinks);

    ui->dirLine->setText(dir);

}

void Window::on_findButton_pressed()
{
    ui->listWidget->clear();

   if(!ui->filtersLine->text().isEmpty()) {
       if (ui->filtersLine->text().at(0) == "*")
       {
          QString temp;
          temp = ui->filtersLine->text().remove(0, 1);
          qDebug() << temp;
          filters << temp;
       }
       else
          filters << ui->filtersLine->text();
   }

   if (ui->sizeCheckBox->isChecked()) {
       if (!ui->sizeSpinBox->text().isEmpty())
            size = ui->sizeSpinBox->text().toInt();
       sizeChecked = true;
   }

   if (ui->ageCheckBox->isChecked()) {
      if (!ui->ageSpinBox->text().isEmpty())
            age = ui->ageSpinBox->text().toInt();
      ageChecked = true;
   }

   if (ui->comboBox->currentText() == "<")
   {
       signInsert = true;
   }

   if (!ui->dirLine->text().isEmpty()) {
       if ((filters.isEmpty()) && (!sizeChecked) && (!ageChecked))
           QMessageBox::warning(this, "Error", "Error! Please, choose any filters to find files!");
       else {
           plugin->findFiles(ui->dirLine->text(), age, size, filters, sizeChecked, ageChecked, signInsert);
       }
   }
   else
       QMessageBox::warning(this, "Error", "Error! Please, select directory!");

   filters.clear();
   age = 0;
   size = 0;
   signInsert = false;
   ageChecked = false;
   sizeChecked = false;
}


void Window::showResults()
{
    qDebug() << "work finished:" << QThread::currentThread();

    result_gui = plugin->future.results();

    foreach(QFileInfo fileInfo, result_gui) {
      stringresult_gui << fileInfo.fileName();
    }

    if (!plugin->watcher.isCanceled()) {
        ui->listWidget->addItem("[ Found: files - " + QString::number(stringresult_gui.size() - plugin->countDirs) + ", dirs - " + QString::number(plugin->countDirs) + ' ]');
        ui->listWidget->addItems(stringresult_gui);
    }

    result_gui.clear();
    stringresult_gui.clear();
}



Window::~Window()
{
    delete ui;
}



