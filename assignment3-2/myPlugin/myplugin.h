#ifndef MYPLUGIN_H
#define MYPLUGIN_H

#include <QObject>
#include <QtPlugin>
#include <QtDebug>
#include <QString>
#include <QDateTime>
#include <QDirIterator>
#include "QtTest/QTest"

#include "myPlugin_global.h"
#include "interface.h"

static int size;
static int age;
static QStringList filters;

static bool sizeChecked;
static bool ageChecked;
static bool signInsert;

class MYPLUGIN_EXPORT Myplugin : public QObject, public Interface
{
  Q_OBJECT

  Q_PLUGIN_METADATA(IID "ru.mephi.mtp")
  Q_INTERFACES(Interface)


public:
  explicit Myplugin(QObject* parent = nullptr);
  ~Myplugin();
  static bool doFilter(const QFileInfo &file);

signals:

public slots:

  // Plugin interface
public:
  void test(QString name) override;
  void findFiles(const QString &startDir, int age, int size, QStringList filters, bool sizeChecked, bool ageChecked, bool signInsert) override;
  QStringList getResults() override;

};


#endif // MYPLUGIN_H
