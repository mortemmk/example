#ifndef INTERFACE_H
#define INTERFACE_H

#include <QtConcurrent/qtconcurrentfilter.h>
#include <QDebug>
#include <QString>
#include <QDir>
#include <QFuture>
#include <QThread>
#include <QFutureWatcher>

class Interface{
public:
  virtual ~Interface(){};
  virtual void test(QString name) = 0;
  virtual void findFiles(const QString &startDir, int age, int size, QStringList filters, bool sizeChecked, bool ageChecked, bool signInsert) = 0;
  virtual QStringList getResults() = 0;

  QFileInfoList filesInfoList;
  QFileInfoList currentResults;

  int countDirs = 0;

  QFuture<QFileInfo> future;
  QFutureWatcher<QFileInfo> watcher;

};


// Decleare interface
Q_DECLARE_INTERFACE(Interface, "ru.mephi.mtp")

#endif // INTERFACE_H
