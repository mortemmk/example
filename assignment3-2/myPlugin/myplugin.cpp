#include "interface.h"
#include "myplugin.h"
#include "qtconcurrentfilter.h"

Myplugin::Myplugin(QObject *parent)
{
    Q_UNUSED(parent);

    connect(&watcher, &QFutureWatcher<void>::resultReadyAt, [=](int index){
        qDebug() << "ready:" << (index + 1);
        currentResults << future.resultAt(index);
        if (future.resultAt(index).isDir())
            countDirs++;
    });

    connect(&watcher, &QFutureWatcher<void>::started, [=](){
        qDebug() << "work started:" << QThread::currentThread();
    });

}

Myplugin::~Myplugin()
{

}

QStringList Myplugin::getResults()
{
    QStringList tempStringRes;
    foreach(QFileInfo fileInfo, currentResults){
        tempStringRes << fileInfo.fileName();
        qDebug() << tempStringRes;
    }
    return tempStringRes;
}

void Myplugin::test(QString name)
{
    qInfo() << "Test from plugin:" << name;
}


 bool Myplugin::doFilter(const QFileInfo &file)
{
     if (sizeChecked && !signInsert && !ageChecked) {
         if(!filters.isEmpty()) {
             foreach(QString filter, filters) {
                if ((file.fileName().contains(filter)) && (file.size() > size))
                    return true;
                }
             }
         else {
             if (file.size() > size)
                  return true;
         }

     }
     if (sizeChecked && signInsert && !ageChecked) {
         if(!filters.isEmpty()) {
             foreach(QString filter, filters) {
                if ((file.fileName().contains(filter)) && (file.size() < size))
                    return true;
                }
            }
         else {
             if (file.size() < size)
                 return  true;
         }
     }
     if (sizeChecked && !signInsert && ageChecked) {
         if(!filters.isEmpty()) {
             foreach(QString filter, filters) {
                if ((file.fileName().contains(filter)) && (file.size() > size) && (file.birthTime() > (QDateTime::currentDateTime().addDays(-age))))
                    return true;
             }
         }
         else {
             if ((file.size() > size) && (file.birthTime() > (QDateTime::currentDateTime().addDays(-age))))
                 return  true;
         }
     }
     if (sizeChecked && signInsert && ageChecked) {
         if(!filters.isEmpty()) {
             foreach(QString filter, filters) {
                if ((file.fileName().contains(filter)) && (file.size() < size) && (file.birthTime() > (QDateTime::currentDateTime().addDays(-age))))
                    return true;
             }
         }
         else {
             if ((file.size() < size) && (file.birthTime() > (QDateTime::currentDateTime().addDays(-age))))
                 return  true;
         }
     }
     if (!sizeChecked && ageChecked) {
         if(!filters.isEmpty()) {
             foreach(QString filter, filters) {
                 if ((file.fileName().contains(filter)) && (file.birthTime() > (QDateTime::currentDateTime().addDays(-age))))
                     return true;
              }
         }
         else {
             if (file.birthTime() > (QDateTime::currentDateTime().addDays(-age)))
                 return  true;
         }
     }
     if (!sizeChecked && !ageChecked) {
         foreach(QString filter, filters) {
            if ((file.fileName().contains(filter)))
                return true;
             }
     }
    // QTest::qWait(1000); // использую для того, чтобы было удобнее тестировать cancelButton
     return false;
}


void Myplugin::findFiles(const QString &startDir, int age, int size, QStringList filters, bool sizeChecked, bool ageChecked, bool signInsert)
{
    ::size = (size*1024);
    ::age = age;
    ::filters = filters;
    ::signInsert = signInsert;

    ::sizeChecked = sizeChecked;
    ::ageChecked = ageChecked;

    currentResults.clear();
    filesInfoList.clear();
    countDirs = 0;

    QDir dir(startDir);

    QDirIterator it(startDir, QDir::AllEntries | QDir::Hidden | QDir::NoSymLinks | QDir::NoDotAndDotDot, QDirIterator::Subdirectories);
      while (it.hasNext())
      {   QFileInfo temp = it.fileInfo();
          filesInfoList << it.next();
      }

    future = QtConcurrent::filtered(filesInfoList, &Myplugin::doFilter);
    watcher.setFuture(future);

}

