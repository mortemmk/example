#include "window.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Window w;

    QDir dir = QDir::current();
    dir.cdUp();
    dir.cd("myplugin");
    dir.cd("debug");
    qInfo() << "Path:" << dir.path();

    QStringList plugins = w.getPlugins(dir.path());
    qInfo() << "Plugins:" << plugins;

    w.testPlugins(plugins);
    qInfo() << "Done!" ;

    w.show();
    return a.exec();
}
